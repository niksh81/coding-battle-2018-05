package com.epam;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GameEngineBasisConfigurationsTest {

    @Test
    void standardAliveSectorTest2() {
        boolean[][] initialState = {
            {false, false, false, false, false},
            {false, false, true, false, false},
            {false, false, false, true, false},
            {false, true, true,  true, false},
            {false, false, false, false, false}
        };
        int numberIterations = 6;

        Game game = new Game();
        boolean[][] expectedState = {
                {false, false, false, true, true},
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false,  false, true},
                {false, false, true, false, true}
        };
        boolean[][] compute = game.compute(initialState, numberIterations);

        assertArrayEquals(expectedState, compute);
        // TODO assertions
    }
}