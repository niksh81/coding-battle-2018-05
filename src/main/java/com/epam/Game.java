package com.epam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Game implements GameEngine {

    static class pp{
        boolean isAlive;
        int neaghbours;
        int x,y;
        public pp(){}
        public pp(pp p){
            x=p.x;
            y=p.y;
            isAlive = p.isAlive;
            neaghbours = p.neaghbours;
        }
        public pp clone(){
            return new pp(this);
        }

    }
    private void checkReview(pp[][]current,pp [][]pps,int x, int y) {
        int a, b;
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                a = (pps.length + i) % pps.length;
                b = (pps[0].length + j) % pps[0].length;
                if (!current[a][b].isAlive && current[a][b].neaghbours == 3){
                    nextAliveList.add(pps[a][b]);
                    pps[a][b].isAlive = true;
                }
            }
        }
    }

    private void allMinus(pp [][]pps,int x, int y) {
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (!(i == x && j == y)){
                    pps[(pps.length + i) % pps.length][(pps[0].length + j) % pps[0].length].neaghbours--;
                }

            }
        }

    }
    private void allPlus(pp [][]pps,int x, int y) {
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                if (!(i == x && j == y)){
                    pps[(pps.length + i) % pps.length][(pps[0].length + j) % pps[0].length].neaghbours++;
                }
            }
        }

    }

    private pp[][] compute(pp[][] matr){
        pp[][] pps = new pp[matr.length][matr[0].length];
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr[0].length; j++) {
                pps[i][j] = matr[i][j].clone();
            }
        }

        for (int i = 0; i < aliveList.size(); i++) {
            pp item = aliveList.get(i);
            checkReview(matr,pps,item.x,item.y);
            if(matr[item.x][item.y].neaghbours<2 || matr[item.x][item.y].neaghbours>3){
                allMinus(pps,item.x,item.y);
                pps[item.x][item.y].isAlive = false;
                aliveList.remove(i--);
            }

        }
        List<pp> list =new ArrayList<>(nextAliveList);
        for (pp p :list) {
            allPlus(pps,p.x,p.y);
            aliveList.add(p);
        }
        nextAliveList.clear();
        return pps;
    }


    static List<pp> aliveList = new ArrayList<>();
    static Set<pp> nextAliveList = new HashSet<>();

    @Override
    public boolean[][] compute(boolean[][] initialState, int numberIterations) {
        pp [][]pps = new pp[initialState.length][initialState[0].length];

        for (int i = 0; i < pps.length; i++) {
            for (int j = 0; j < pps[i].length; j++) {
                pps[i][j] = new pp();
                pps[i][j].x=i;
                pps[i][j].y=j;
            }
        }
        for (int i = 0; i < pps.length; i++) {
            for (int j = 0; j < pps[i].length; j++) {
                if (initialState[i][j]){
                    allPlus(pps,i,j);
                    pps[i][j].isAlive = initialState[i][j];
                    aliveList.add(pps[i][j]);
                }
            }
        }
        for (int i = 0; i < numberIterations; i++) {
            pps = compute(pps);
        }
        for (int i = 0; i < pps.length; i++) {
            for (int j = 0; j < pps[i].length; j++) {
                initialState[i][j] = pps[i][j].isAlive;
            }
        }
        return initialState;
    }

}